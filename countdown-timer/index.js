const daysEl=document.getElementById("days");
const hoursEl=document.getElementById("hours");
const minsEl=document.getElementById("mins");
const secondsEl=document.getElementById("seconds");


const newYear="1 jan 2023";

const countDown=()=>{
    const newYearDate=new Date(newYear);
    const currentDate=new Date();
    const totalSeconds=(newYearDate-currentDate)/1000;
    const days=Math.floor(totalSeconds/3600/24);
    const hours=Math.floor(totalSeconds/3600)%24;
    const mins=Math.floor(totalSeconds/60)%60;
    const seconds=Math.floor(totalSeconds)%60;

    daysEl.innerHTML=days;
    hoursEl.innerHTML=FormTime(hours);
    minsEl.innerHTML=FormTime(mins);
    secondsEl.innerHTML=FormTime(seconds);
}

const FormTime=(time)=>{
    if(time<10)
    return `0${time}`;
    else
    return time;
}

countDown();

setInterval(countDown,1000);